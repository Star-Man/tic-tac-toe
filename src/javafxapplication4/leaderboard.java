package javafxapplication4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.layout.FlowPane;
import static javafxapplication4.GameSelect.logged_in_user_id;
import static javafxapplication4.GameSelect.logged_in_username;

public class leaderboard{
    
    
    private TTTWebService TTTWebServiceProxy;



    @FXML
    private FlowPane leaderboard_pane;

    @FXML
    protected void return_to_main(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game_select.fxml"));

        Parent root = fxmlLoader.load();

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Game Select");

        window.show();

    }
    
    @FXML
    public void initialize() {
        TTTWebService_Service service = new TTTWebService_Service();
        TTTWebServiceProxy = service.getTTTWebServicePort();
        String league_table = TTTWebServiceProxy.leagueTable();


        if ("ERROR-NOGAMES".equals(league_table)){
            TextField text = new TextField();
            text.setEditable(false);            
            text.setText("There are no games");
            leaderboard_pane.getChildren().add(text);     
        }
        else if ("ERROR-NOGAMES".equals(league_table)){
            TextField text = new TextField();
            text.setEditable(false);                        
            text.setText("Unable to access database");
            leaderboard_pane.getChildren().add(text);   
        }
        else{
            String[] league_list = league_table.split("\\r?\\n");
            List<String> players = new ArrayList<String>();
            List<String> status = new ArrayList<String>();

            
            for (String s: league_list) {        
                //Split single into array game id, username, and time

                String[] split_game = s.split(",");
                String game_id = split_game[0];
                String player1 = split_game[1];
                String player2 = split_game[2];
                String game_state = split_game[3];
                if (!players.contains(player1)){
                    players.add(player1);
                }
                if (!players.contains(player2)){
                    players.add(player2);
                }            

                
                if ("1".equals(game_state)){
                    status.add("WIN: " + player1);
                    status.add("LOSS: " + player2);                     
                }
                if ("2".equals(game_state)){
                    status.add("WIN: " + player2);
                    status.add("LOSS: " + player1);                  
                }                
                if ("3".equals(game_state)){
                    status.add("TIE: " + player1);
                    status.add("TIE: " + player2);                                                              
                }                    
            } 
            for (String s: players) {           
                int wins = 0;
                int losses = 0;
                int ties = 0;
                for (String x: status) {        
                    if(x.equals("WIN: " +s)){
                        wins++;
                    }
                    if(x.equals("LOSS: " +s)){
                        losses++;
                    }
                    if(x.equals("TIE: " +s)){
                        ties++;
                    }                    
                }
                
                TextField new_game_text = new TextField();
                new_game_text.setEditable(false);                
                new_game_text.setPrefWidth(487);                              
                new_game_text.setText("Player: " + s + ". Wins: " + wins + ". Losses: " + losses + ". Ties: " + ties);               
                leaderboard_pane.getChildren().add(new_game_text);                  
                
            }               
        }  
    }    
}