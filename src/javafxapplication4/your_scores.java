package javafxapplication4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.layout.FlowPane;

public class your_scores{
    
    
    public static String logged_in_username = "";
    public static String logged_in_user_id = "";

    private TTTWebService TTTWebServiceProxy;

    public static void get_username(String username){
        logged_in_username = username;
    }
    
    public static void get_logged_in_user_id(String id) {
        logged_in_user_id = id;
    }    
    
    @FXML
    private FlowPane games_pane;
    @FXML
    private TextField wins_box;
    @FXML
    private TextField ties_box;
    @FXML
    private TextField losses_box;

    
    @FXML
    protected void return_to_main(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game_select.fxml"));

        Parent root = fxmlLoader.load();

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Game Select");

        window.show();

    }
    
    @FXML
    public void initialize() {
        TTTWebService_Service service = new TTTWebService_Service();
        TTTWebServiceProxy = service.getTTTWebServicePort();
        String league_table = TTTWebServiceProxy.leagueTable();
        //Get the league table
        System.out.println(league_table);
            
            

        //Check for errors in output
        if ("ERROR-NOGAMES".equals(league_table)){
            TextField text = new TextField();
            text.setEditable(false);            
            text.setText("There are no games");
            games_pane.getChildren().add(text);     
        }
        else if ("ERROR-NOGAMES".equals(league_table)){
            TextField text = new TextField();
            text.setEditable(false);            
            text.setText("Unable to access database");
            games_pane.getChildren().add(text);   
        }
        else{
            //No errors in output
            //Split up league list by line
            String[] league_list = league_table.split("\\r?\\n");
            int wins = 0;
            int losses = 0;
            int ties = 0;

            for (String s: league_list) {        
                //Split single into array game id, player1, player2, game_state

                
                String[] split_game = s.split(",");
                String game_id = split_game[0];
                String player1 = split_game[1];
                String player2 = split_game[2];
                String game_state = split_game[3];

                if((player1.equals(logged_in_username) && "1".equals(game_state)) || (player2.equals(logged_in_username) && "2".equals(game_state))){
                    wins++;    
                }
                if ((player1.equals(logged_in_username) && "2".equals(game_state)) || (player2.equals(logged_in_username) && "1".equals(game_state))){
                    losses++;                    
                }
                if((player1.equals(logged_in_username) || player2.equals(logged_in_username) ) && "3".equals(game_state)){                    
                    ties++;               
                }
                
                          
                if(player1.equals(logged_in_username) || player2.equals(logged_in_username)){
                               
                    if ("1".equals(game_state)){
                        TextField new_game_text = new TextField();
                        new_game_text.setPrefWidth(487);  
                        new_game_text.setEditable(false);            
                        
                        new_game_text.setText("Game: " + game_id + " Between " + player1 + " And " + player2 + ". Won by: " + player1);
                        games_pane.getChildren().add(new_game_text);   

                    }
                    if ("2".equals(game_state)){
                        TextField new_game_text = new TextField();
                        new_game_text.setPrefWidth(487);      
                        new_game_text.setEditable(false);            
                        
                        new_game_text.setText("Game: " + game_id + " Between " + player1 + " And " + player2 + ". Won by: " + player2);
                        games_pane.getChildren().add(new_game_text);   

                    }                
                    if ("3".equals(game_state)){
                        TextField new_game_text = new TextField();
                        new_game_text.setEditable(false);            
                        
                        new_game_text.setPrefWidth(487);                              
                        new_game_text.setText("Game: " + game_id + " Between " + player1 + " And " + player2 + ". Tie.");
                        games_pane.getChildren().add(new_game_text);                       
                    }                    

                }

            }   
            
            wins_box.setText("Wins: "+String.valueOf(wins));
            ties_box.setText("Ties: "+String.valueOf(ties));
            losses_box.setText("Losses: "+String.valueOf(losses));
        
        
        }    
    } 
    
    
}