package javafxapplication4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;


public class login_user {

    
    private TTTWebService TTTWebServiceProxy;
    
    
    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;



    @FXML
    private Button login_button;

    @FXML
    private Button register_button;

    public login_user() {
    }




    @FXML
    protected void register(ActionEvent event) throws IOException {
        System.out.println("Clicked register button");

        Parent register_parent = FXMLLoader.load(getClass().getResource("register.fxml"));
        Scene register_scene = new Scene(register_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Registration");

        window.show();
    }


    @FXML
    protected void login(ActionEvent event) throws Exception {
        String username = username_field.getText();
        String password = password_field.getText();


        if(username.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta have a name");

            alert.showAndWait();
        }


        else if(password.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password");

            alert.showAndWait();
        }


        else {
            System.out.println("All data entered");

            
            
            
            TTTWebService_Service service = new TTTWebService_Service();
            TTTWebServiceProxy = service.getTTTWebServicePort();
            int login_check = TTTWebServiceProxy.login(username, password);
                    
                
            if (login_check == -1) {
                      
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Login Details Incorrect");

                alert.showAndWait();
            }
            else if (login_check != 0){
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game_select.fxml"));

                GameSelect.get_user_id(String.valueOf(login_check));
                GameSelect.get_username(username);

                
                Parent root = fxmlLoader.load();

                Scene register_scene = new Scene(root);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(register_scene);
                window.setTitle("Tic Tac Toe Game Select");

                window.show();
            
            }
            else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Login Error");

                alert.showAndWait();
            }


        }



    }
}