package javafxapplication4;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javafx.scene.layout.FlowPane;

public class GameSelect{
    
    
    private TTTWebService TTTWebServiceProxy;

    public static String logged_in_user_id = "";
    public static String logged_in_username = "";


    public static void get_user_id(String uid){
        logged_in_user_id = uid;

    }

    public static void get_username(String username){
        logged_in_username = username;

    }

    
    @FXML
    private FlowPane scroll_area;

    

    
    @FXML
    protected void leaderboard_pressed(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/leaderboard.fxml"));

        Parent root = fxmlLoader.load();

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Scores");

        window.show();

    }    
    
    
    @FXML
    protected void your_scores_pressed(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/your_scores.fxml"));
        your_scores.get_username(logged_in_username);
        your_scores.get_logged_in_user_id(logged_in_user_id);

        Parent root = fxmlLoader.load();

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Scores");

        window.show();

    }
    

    
    
    @FXML
    protected void create_game(ActionEvent event) throws Exception {

        TTTWebService_Service service = new TTTWebService_Service();
        TTTWebServiceProxy = service.getTTTWebServicePort();
        String new_game = TTTWebServiceProxy.newGame(Integer.parseInt(logged_in_user_id));
        
        if ("ERROR-NOTFOUND".equals(new_game)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Cannot find ID of added game");
            alert.showAndWait();
        }
        else if ("ERROR-RETRIEVE".equals(new_game)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Cannot access games table");
            alert.showAndWait();
        }
        else if ("ERROR-INSERT".equals(new_game)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Cannot add new game to system");
            alert.showAndWait();
        }        
        else if ("ERROR-DB".equals(new_game)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Cannot access database");
            alert.showAndWait();
        }   
        else{
            System.out.println("Created game" + new_game);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game.fxml"));

            Game.get_logged_in_user_id(logged_in_user_id);
            Game.get_logged_in_username(logged_in_username);
            
            Game.get_gid(new_game);
            Parent root = fxmlLoader.load();

            Scene register_scene = new Scene(root);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(register_scene);
            window.setTitle("Tic Tac Toe");

            window.show();
        }
        
   

    }
    
    

    @FXML
    public void initialize() {


            TTTWebService_Service service = new TTTWebService_Service();
            TTTWebServiceProxy = service.getTTTWebServicePort();
            String open_games = TTTWebServiceProxy.showOpenGames();
            String all_my_games = TTTWebServiceProxy.showAllMyGames(Integer.parseInt(logged_in_user_id));
            
            
            TextField new_game_text = new TextField();
            new_game_text.setText("Games waiting for second player");
            new_game_text.setPrefWidth(233);             
            new_game_text.setEditable(false);   
            
            
            scroll_area.getChildren().add(new_game_text);     
            if ("ERROR-DB".equals(open_games)  || "ERROR-DB".equals(all_my_games)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("ERROR: Cannot access database");
                alert.showAndWait();
            }
            
            
            if ("ERROR-NOGAMES".equals(open_games)){
                TextField text_field = new TextField();
                text_field.setText("There are no games");
                text_field.setEditable(false);   
                text_field.setPrefWidth(233);             
               
                scroll_area.getChildren().add(text_field);    
            }
            else{
                String[] open_games_list = open_games.split("\\r?\\n");
                for (String s: open_games_list) {        
                    //Split single into array game id, username, and time

                    String[] split_game = s.split(",");
                    String game_id = split_game[0];
                    String user = split_game[1];
                    String time = split_game[2];

                    
                    //Only show other users
                    if(!user.equals(logged_in_username)){
                        Button text_field = new Button();
                        text_field.setPrefWidth(233);             

                        text_field.addEventHandler(ActionEvent.ACTION, (ActionEvent actionEvent) -> {

                            TTTWebServiceProxy = service.getTTTWebServicePort();
                            String join_game = TTTWebServiceProxy.joinGame(Integer.parseInt(logged_in_user_id), Integer.parseInt(game_id)); 
                            
                            if("0".equals(join_game)){
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText(null);
                                alert.setContentText("Join game unsuccessful");
                                alert.showAndWait();                           
                            }
                            else if ("ERROR".equals(join_game)){
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText(null);
                                alert.setContentText("ERROR: Database not found");
                                alert.showAndWait();                                                                                       

                            }
                            else{
                                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game.fxml"));


                                
                                Game.get_logged_in_user_id(logged_in_user_id);
                                Game.get_gid(game_id);
                                Game.get_logged_in_username(logged_in_username);
                                
                                Parent root = null;
                                try {
                                    root = fxmlLoader.load();
                                } catch (IOException ex) {
                                    Logger.getLogger(GameSelect.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                Scene register_scene = new Scene(root);
                                Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
                                window.setScene(register_scene);
                                window.setTitle("Tic Tac Toe");

                                window.show();
                            }

                        });        


                        text_field.setText( "Game " + game_id + " started by " + user );
                        scroll_area.getChildren().add(text_field);                    
                    }

                    
                }
            }

            TextField active_game_text = new TextField();
            active_game_text.setEditable(false);
            active_game_text.setPrefWidth(233);             

            active_game_text.setText("Your games");
            scroll_area.getChildren().add(active_game_text);   
            
            
            if ("ERROR-NOGAMES".equals(all_my_games)){
                TextField text_field = new TextField();
                text_field.setText("There are no games");
                text_field.setPrefWidth(233);                            
                text_field.setEditable(false);   
                
                scroll_area.getChildren().add(text_field);
            }
            else{
                String[] all_my_games_list = all_my_games.split("\\r?\\n");

                for (String s: all_my_games_list) {
                    //Split single into array game id, username, and time

                    String[] split_game = s.split(",");
                    String game_id = split_game[0];
                    String user1 = split_game[1];
                    String user2 = split_game[2];

                    if(split_game.length == 4 ){
                        Button text_field = new Button();
                        text_field.setPrefWidth(233);             
                        text_field.addEventHandler(ActionEvent.ACTION, (ActionEvent actionEvent) -> {


                            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game.fxml"));

                            Game.get_logged_in_username(logged_in_username);                               
                            Game.get_logged_in_user_id(logged_in_user_id);
                            Game.get_gid(game_id);


                            Parent root = null;
                            try {
                                root = fxmlLoader.load();
                            } catch (IOException ex) {
                                Logger.getLogger(GameSelect.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            Scene register_scene = new Scene(root);
                            Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
                            window.setScene(register_scene);
                            window.setTitle("Tic Tac Toe");

                            window.show();


                        });        


                        text_field.setText( "Game " + game_id + " played by " + user1 + " and " + user2 );
                        scroll_area.getChildren().add(text_field);                        
                    }
                }            
            
            }
            if ("ERROR-DB".equals(open_games)  || "ERROR-DB".equals(all_my_games)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("ERROR: Cannot access database");
                alert.showAndWait();
            }                 
    } 
}